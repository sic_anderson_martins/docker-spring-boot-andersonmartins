FROM openjdk:12
ADD target/docker-spring-boot-anderson_martins.jar docker-spring-boot-anderson_martins.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-anderson_martins.jar"]
